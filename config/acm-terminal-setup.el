;;; acm-terminal-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 06/12/2022
;;; Loads acm-terminal
;;; Code:

(use-package acm-terminal
  :if (not (display-graphic-p))
  :after (lsp-bridge popon)
	:ensure (:type git :host github :repo "twlz0ne/acm-terminal"
			           :files (:defaults
				                 "*")
			           :includes (acm-terminal.el)))

(provide 'acm-terminal-setup)
;;; acm-terminal-setup.el ends here
