;;; adaptive-wrap-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 28/06/2023
;;; Set the wrap-prefix property on the fly so that single-long-line paragraphs get
;;; displayed nicely without actually changing the buffer's text
;;; Code:

(use-package adaptive-wrap
  :hook (prog-mode . adaptive-wrap-prefix-mode) )

(provide 'adaptive-wrap-setup)
;;; adaptive-wrap-setup.el ends here
