;;; apheleia-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the apheleia mode and configure it
;;; Code:

;; Format all languages
(use-package apheleia
  :config
  ;; make prettier use prettierd by default
  (setf (alist-get 'prettier apheleia-formatters)
        '("apheleia-npx" "prettierd" filepath))

  ;; Set prettier as the default formatter for java-mode and
  ;; java-ts-mode (prettier-java plugin needed)
  (setf (alist-get 'java-mode apheleia-mode-alist)
        'prettier)
  (setf (alist-get 'java-ts-mode apheleia-mode-alist)
        'prettier)

  ;; always use apheleia
  (apheleia-global-mode +1))

(provide 'apheleia-setup)
;;; apheleia-setup.el ends here
