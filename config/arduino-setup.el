;;; arduino-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the arduino mode and configure it
;;; Code:

;; Arduino
(use-package arduino-mode
  :defer t
  :hook arduino-mode)

(provide 'arduino-setup)
;;; arduino-setup.el ends here
