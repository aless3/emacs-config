;;; auto-sudoedit-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads auto-sudoedit
;;; Code:

(use-package auto-sudoedit
  :config
  (auto-sudoedit-mode 1))

(provide 'auto-sudoedit-setup)
;;; auto-sudoedit-setup.el ends here
