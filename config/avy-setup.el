;;; avy-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads avy to easily jump around based on text
;;; Code:

(use-package avy
  :config
  ;; change avy keys to match home row on dvorak
  (unless block-xah
    (setq avy-keys '(?a ?o ?e ?u ?i ?d ?h ?t ?n ?s))))

(provide 'avy-setup)
;;; avy-setup.el ends here
