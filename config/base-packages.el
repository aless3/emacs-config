;;; base-packages.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads package managers and some necessary packages
;;; Code:

;; QoL packages
(require 'no-littering-setup)
(require 'whitespace-setup)
(require 'which-key-setup)
(require 'copy-paste-setup)
(require 'undo-setup)
(require 'posframe-setup)
(require 'deadgrep-setup)
(require 'wgrep-setup)
(require 'hl-line-setup)
(require 'hungry-delete-setup)
(require 'zoom-setup)
(require 'adaptive-wrap-setup)
(require 'beginend-setup)
(require 'iedit-setup)
(require 'dumb-jump-setup)
(require 'avy-setup)

;; exec-path-from shell was misbehaving, this hack seems to mollify it
(use-package exec-path-from-shell
  :init
  (setq exec-path-from-shell-arguments nil)
  (exec-path-from-shell-initialize))

;; (require 'ws-butler-setup)


;; GUI
(require 'icons-setup)
(require 'theme-setup)
(require 'font-setup)
(require 'mode-line-setup)
(require 'paren-setup)
(require 'highlight-indent-guides-setup)
;; (require 'windmove-setup)
;; (require 'winner-setup)
(require 'switch-window-setup)

(provide 'base-packages)
;;; base-packages.el ends here
