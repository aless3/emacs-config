;;; base-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads base utilities - no modes and before package managers
;;; Code:

;; Custom file
(require 'custom-setup)

;; Constants
(require 'constants-setup)

;; Qol
(require 'qol-setup)

;; Bindings
(require 'unbind-setup)
(require 'bind-global-setup)


(provide 'base-setup)
;;; base-setup.el ends here
