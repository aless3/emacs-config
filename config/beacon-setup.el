;;; beacon-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads beacon
;;; Code:

(use-package beacon
  :config
  (beacon-mode 1))

(provide 'beacon-setup)
;;; beacon-setup.el ends here
