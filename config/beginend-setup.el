;;; beginend-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 11/07/2023
;;; Navigate to meaningful beginning and end of buffers when there are comments
;;; in those spots, jumping to code
;;; Code:

(use-package beginend
  :config
  (beginend-global-mode))

(provide 'beginend-setup)
;;; beginend-setup.el ends here
