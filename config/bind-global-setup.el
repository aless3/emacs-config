;;; bind-global-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Bind some global bindings
;;; Code:

;; Next buffer
(global-set-key (kbd "C-<right>") nil)
(global-set-key (kbd "C-<right>") 'next-buffer)

;; Previous buffer
(global-set-key (kbd "C-<left>") nil)
(global-set-key (kbd "C-<left>") 'previous-buffer)

;; Use paste (yank) with C-v, still keeping C-y
(global-set-key (kbd "C-y") 'yank)
(global-set-key (kbd "C-v") 'yank)

(provide 'bind-global-setup)
;;; bind-global-setup.el ends here
