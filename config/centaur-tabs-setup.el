;;; centaur-tabs-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 19/07/2023
;;;
;;; Code:

(use-package centaur-tabs
  :defer 0
  :config
  (centaur-tabs-mode t)
  (advice-add 'next-buffer :override #'centaur-tabs-forward-tab)
  (advice-add 'previous-buffer :override #'centaur-tabs-backward-tab)
  :bind
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward))

(provide 'centaur-tabs-setup)
;;; centaur-tabs-setup.el ends here
