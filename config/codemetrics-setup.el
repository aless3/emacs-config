;;; codemetrics-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 27/06/2023
;;; Shows live calculation of the Cognitive Complexity metric
;;; Code:

(use-package codemetrics
  :defer t
  :ensure (codemetrics :type git :host github :repo "jcs-elpa/codemetrics"))

(provide 'codemetrics-setup)
;;; codemetrics-setup.el ends here
