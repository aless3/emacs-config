;;; comment-config-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 27/06/2023
;;; Comment made easy: comment full half-selected lines if more lines are selected
;;; to avoid splitting a line in the middle
;;; Code:

(use-package evil-nerd-commenter)

(provide 'comment-config-setup)
;;; comment-config-setup.el ends here
