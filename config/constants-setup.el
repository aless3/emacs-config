;;; constants-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Configure useful constants
;;; Code:


;; Useful constants
(defconst *sys/win32*
  (eq system-type 'windows-nt)
  "Are we running on a WinTel system?")

(defconst *sys/linux*
  (eq system-type 'gnu/linux)
  "Are we running on a GNU/Linux system?")

(defconst *sys/mac*
  (eq system-type 'darwin)
  "Are we running on a Mac system?")

(defconst python-p
  (or (executable-find "python3")
      (and (executable-find "python")
           (> (length (shell-command-to-string "python --version | grep 'Python 3'")) 0)))
  "Do we have python3?")

(defconst pip-p
  (or (executable-find "pip3")
      (and (executable-find "pip")
           (> (length (shell-command-to-string "pip --version | grep 'python 3'")) 0)))
  "Do we have pip3?")

(defconst clangd-p
  (or (executable-find "clangd")  ;; usually
      (executable-find "/usr/local/opt/llvm/bin/clangd"))  ;; macOS
  "Do we have clangd?")

;; If you want to disable xah and dvorak setup create
;; an empty file called 'config/block-xah' in your
;; Emacs user directory.
(defconst block-xah
  (file-exists-p (expand-file-name "config/block-xah" user-emacs-directory)))

;; Disable install & use of fun modes.
(defconst block-fun
  (file-exists-p (expand-file-name "config/block-fun" user-emacs-directory)))

(provide 'constants-setup)
;;; constants-setup.el ends here
