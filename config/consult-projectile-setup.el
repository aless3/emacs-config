;;; consult-projectile-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the consult-projectile mode and configure it
;;; Code:

(use-package consult-projectile
  :ensure (consult-projectile :type git :host gitlab :repo "OlMon/consult-projectile" :branch "master")
  :after (consult projectile))

(provide 'consult-projectile-setup)
;;; consult-projectile-setup.el ends here
