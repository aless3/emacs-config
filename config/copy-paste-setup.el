;;; copy-paste-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Utilities for copying and pasting
;;; Code:

;; Whole-line-or-region - use whole line for cut/copy when no text is selected
(use-package whole-line-or-region
  :config
  (add-to-list 'whole-line-or-region-inhibit-modes 'comint-mode)
  (whole-line-or-region-global-mode t))

;; Popup-kill-ring - better killing means better paste!
(use-package popup-kill-ring
  :bind ("M-v" . popup-kill-ring)) ; M-v paste with kill ring

(provide 'copy-paste-setup)
;;; copy-paste-setup.el ends here
