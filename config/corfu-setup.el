;;; corfu-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 30/07/2024
;;; Corfu completion and extensions (cape)
;;; Code:

(use-package corfu
  :custom
  (corfu-cycle nil)
  (corfu-separator ?\s)

  (corfu-quit-at-boundary t)
  (corfu-quit-no-match t)

  (corfu-auto t)
  (corfu-auto-delay 0.25)
  (corfu-auto-prefix 1)

  (corfu-popupinfo-delay= '(1 . 0.25))
  (corfu-popupinfo-direction '(force-right))

  :bind
  (:map corfu-map
        ("C-u" . corfu-previous)
        ("C-t" . corfu-next)
        ("<home>" . corfu-quit))
  :init
  (global-corfu-mode)
  (corfu-popupinfo-mode)
  (corfu-history-mode)
  (corfu-indexed-mode))

;; Add extensions
(use-package cape
  :init
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-history)
  (add-hook 'completion-at-point-functions #'cape-keyword))

(provide 'corfu-setup)
;;; corfu-setup.el ends here
