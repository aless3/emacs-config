;;; crdt.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the crdt mode and configure it
;;; Code:

(use-package crdt
  :config
  (setq crdt-tuntox-executable "tuntox")
  (setq crdt-use-tuntox t))


(provide 'crdt-setup)
;;; crdt-setup.el ends here
