;;; custom-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Custom file configs
;;; Code:

;;; First of all disable loading the custom file to keep this file tidy
(setq custom-file (make-temp-file "emacs-custom"))
(load custom-file 'noerror)

(provide 'custom-setup)
;;; custom-setup.el ends here
