;;; deadgrep-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads deadgrep
;;; Code:

(use-package deadgrep
  :config
  (global-set-key (kbd "<f6>") nil)
  (global-set-key (kbd "<f6>") #'deadgrep))

(provide 'deadgrep-setup)
;;; deadgrep-setup.el ends here
