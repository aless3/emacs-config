;;; diff-hl-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads diff-hl to highlights uncommitted changes on the margin
;;; Code:

(use-package diff-hl
  :config
  (diff-hl-margin-mode)
  (diff-hl-flydiff-mode)
  (diff-hl-show-hunk-mouse-mode)
  (global-diff-hl-mode))


(provide 'diff-hl-setup)
;;; diff-hl-setup.el ends here
