;;; diminish-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the diminish mode and configure it
;;; Code:

(use-package diminish
  :diminish)

(provide 'diminish-setup)
;;; diminish-setup.el ends here
