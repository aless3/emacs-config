;;; dirvish-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the dirvish mode and configure it
;;; Code:

(use-package dirvish
  :config
  (dirvish-override-dired-mode)
  ;; (dirvish-peek-mode) ; Preview files in minibuffer
  (dirvish-side-follow-mode) ; similar to `treemacs-follow-mode'
  (setq dirvish-mode-line-format
        '(:left (sort symlink) :right (omit yank index)))
  ;; (setq dirvish-attributes
        ;; '(all-the-icons file-time file-size collapse subtree-state vc-state git-msg))
  (setq dired-listing-switches
  "-l --almost-all --human-readable --group-directories-first --no-group")

  :bind ; Bind `dirvish|dirvish-side|dirvish-dwim' as you see fit
  (
   ("C-c f" . dirvish-fd)
   ;;("C-0" . dirvish-side)
   ("M-0" . dirvish-side)
   :map dirvish-mode-map ; Dirvish inherits `dired-mode-map'
   ("C"   . dirvish-quick-access)
   ("a"   . dirvish-preview-dispatchers)

   ("t"   . dired-next-line)
   ("c"   . dired-previous-line)

   ("h"   . dired-up-directory)
   ("n"   . dired-find-file)

   ("f"   . dirvish-file-info-menu)
   ("k"   . dirvish-yank-menu)
   ("N"   . dirvish-narrow)

   ("b"   . dirvish-history-jump)

   ("s"   . dirvish-quicksort)    ; remapped `dired-sort-toggle-or-edit'
   ("v"   . dirvish-vc-menu)      ; remapped `dired-view-file'
   ("TAB" . dirvish-subtree-toggle)
   ("SPC" . dirvish-subtree-toggle)))

(provide 'dirvish-setup)
;;; dirvish-setup.el ends here
