;;; docker-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the docker utility modes and configure it
;;; Code:

;; Docker
(use-package docker
  :defer t)

;; Dockerfile
(use-package dockerfile-mode
  :defer t)

(provide 'docker-setup)
;;; docker-setup.el ends here
