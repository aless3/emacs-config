;;; dumb-jump-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 27/07/2023
;;; Dumb-jump is a package to easily navigate xref(s)
;;; Code:

(use-package dumb-jump
  :custom
  (dumb-jump-prefer-searcher 'rg)
  (xref-show-definitions-function #'consult-xref)
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

(provide 'dumb-jump-setup)
;;; dumb-jump-setup.el ends here
