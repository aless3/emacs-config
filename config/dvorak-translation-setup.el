;;; dvorak-translation-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Swap some keys for better ergonomic in dvorak
;;; Code:

;;; Translation map for Dvorak for 'C-x' and 'C-c'
(define-key key-translation-map [?\C-x] [?\C-n])
(define-key key-translation-map [?\C-n] [?\C-x])

(define-key key-translation-map [?\C-u] [?\C-c])
(define-key key-translation-map [?\C-c] [?\C-u])

;; since 'C-u' is now being used as 'C-c'
;; set universal-argument on 'C-b'
(global-set-key (kbd "C-b") nil)
(global-set-key (kbd "C-b") 'universal-argument)

(provide 'dvorak-translation-setup)
;;; dvorak-translation-setup.el ends here
