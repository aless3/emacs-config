;;; elixir-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 11/06/2024
;;;
;;; Code:

(use-package elixir-ts-mode
  :ensure t
  :custom
  (lsp-elixir-server-command '("~/lexical/_build/dev/package/lexical/bin/start_lexical.sh")))


(provide 'elixir-setup)
;;; elixir-setup.el ends here
