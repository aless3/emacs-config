;;; esup-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 30/07/2024
;;; Emacs Start Up Profiler - restart Emacs and find slow staring problems
;;; Code:

(use-package esup)

(provide 'esup-setup)
;;; esup-setup.el ends here
