;;; expand-region-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 17/05/2023
;;;
;;; Code:

(use-package expand-region
  :bind ("C-=" . er/expand-region))

(provide 'expand-region-setup)
;;; expand-region-setup.el ends here
