;;; file-utils-packages.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads packages that work with files
;;; Code:

(require 'projectile-setup)
(require 'auto-sudoedit-setup)
(require 'dirvish-setup)

(require 'pdf-setup)
;; (require 'latex-setup)

(provide 'file-utils-packages)
;;; file-utils-packages.el ends here
