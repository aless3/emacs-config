;;; flycheck-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the flycheck mode and configure it
;;; Code:

;; Flycheck
(use-package flycheck
  :init (global-flycheck-mode)
  :config
  (add-hook 'elpaca-after-init-hook #'global-flycheck-mode)
  (global-set-key (kbd "C-c C-c") nil)
  (global-set-key (kbd "C-c c l") 'flycheck-list-errors)
  (global-set-key (kbd "C-c c n") 'flycheck-next-error)
  (global-set-key (kbd "C-c c p") 'flycheck-previous-error)
  ;; Easy Shortcuts
  (global-set-key (kbd "C-c n") 'flycheck-next-error)
  (global-set-key (kbd "C-c h") 'flycheck-previous-error)
  (global-set-key (kbd "C-c t") 'flycheck-list-errors))

(provide 'flycheck-setup)
;;; flycheck-setup.el ends here
