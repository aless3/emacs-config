;;; font-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Configure the font(s)
;;; Code:

;; Fonts - default Hack 17
(defvar font-list '(("Hack" . 17) ("Input" . 15) ("Consolas" . 16) ("Love LetterTW" . 17))
  "List of fonts and sizes.  The first one available will be used.")

;; other computers may need different sizes:
(cond ((string= (system-name) "dino") ;; dino uses hack size 16
       (setf font-list '(("Hack" . 17) ("Input" . 16) ("Consolas" . 17) ("Love LetterTW" . 18)))))

;; Change font utility
(defun change-font ()
  "Interactively change a font from a list a available fonts."
  (interactive)
  (let* (available-fonts font-name font-size font-setting)
    (dolist (font font-list (setq available-fonts (nreverse available-fonts)))
      (when (member (car font) (font-family-list))
        (push font available-fonts)))
    (if (not available-fonts)
        (message "No fonts from the chosen set are available")
      (if (called-interactively-p 'interactive)
          (let* ((chosen (assoc-string (completing-read "What font to use? " available-fonts nil t) available-fonts)))
            (setq font-name (car chosen) font-size (read-number "Font size: " (cdr chosen))))
        (setq font-name (caar available-fonts) font-size (cdar available-fonts)))
      (setq font-setting (format "%s-%d" font-name font-size))
      (set-frame-font font-setting nil t)
      (add-to-list 'default-frame-alist (cons 'font font-setting)))))

(when (display-graphic-p)
  (change-font))

;; Also change font in emacsclient by adding the hook
(defun emacsclient-change-font (frame)
  "Change the font of frame FRAME."
  (progn
    (select-frame frame)
    (change-font)))

(if (daemonp)
    (add-hook 'after-make-frame-functions 'emacsclient-change-font))

(provide 'font-setup)
;;; font-setup.el ends here
