;;; fun-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads fun things
;;; Code:

(require 'xkcd-setup)

(provide 'fun-setup)
;;; fun-setup.el ends here
