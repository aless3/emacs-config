;;; general-packages.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads general packages that are very useful
;;; Code:

(require 'consult-setup)
(require 'vertico-setup)
(require 'embark-setup)
(require 'marginalia-setup)
(require 'orderless-setup)

(require 'emacs-package-setup)
(require 'embark-consult-setup)
(require 'consult-projectile-setup)

(require 'vterm-setup)
(require 'crdt-setup)

(require 'esup-setup)

(provide 'general-packages)
;;; general-packages.el ends here
