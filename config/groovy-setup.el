;;; groovy-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the groovy mode and configure it
;;; Code:

;; Groovy & Grails
(use-package groovy-mode
  :defer t)

(provide 'groovy-setup)
;;; groovy-setup.el ends here
