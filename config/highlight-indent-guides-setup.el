;;; highlight-indent-guides-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the highlight-indent-guides mode and configure it
;;; Code:

;; Highlight-indent-guides
(use-package highlight-indent-guides
  ;; Enable manually if needed, it a severe bug which potentially core-dumps Emacs
  ;; https://github.com/DarthFennec/highlight-indent-guides/issues/76
  :commands (highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-auto-enabled nil)
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-responsive 'top)

  (set-face-background 'highlight-indent-guides-odd-face "darkgray")
  (set-face-background 'highlight-indent-guides-even-face "dimgray")
  (set-face-foreground 'highlight-indent-guides-character-face "dimgray")

  ;; This is useful to see the current indentation level when scrolling the code
  (setq highlight-indent-guides-delay 0)

  :hook (prog-mode . highlight-indent-guides-mode))

(provide 'highlight-indent-guides-setup)
;;; highlight-indent-guides-setup.el ends here
