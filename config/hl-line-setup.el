;;; hl-line-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads hl-line to never lose the cursor again!
;;; Code:

(require 'hl-line+)

(setq hl-line-flash-show-period 0.5)
(setq hl-line-inhibit-highlighting-for-modes '(dired-mode))
(setq hl-line-overlay-priority -100) ;; sadly, seems not observed by dired

;; disable standard hl-line-mode to use hl-line+.el
(global-hl-line-mode -1)

;; Hooks
(add-hook 'post-command-hook 'hl-line-flash)
(add-hook 'focus-in-hook 'hl-line-flash) ; should use `after-focus-change-function`, but it throws a warning now

(provide 'hl-line-setup)
;;; hl-line-setup.el ends here
