;;; hungry-delete-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads hungry-delete to simplify soft-tab deletions
;;; Code:

(use-package hungry-delete
  :config
  (setq hungry-delete-join-reluctantly t)
  :hook prog-mode)


(provide 'hungry-delete-setup)
;;; hungry-delete-setup.el ends here
