;;; icons-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the icon modes and configure them
;;; Code:

;; ;; various icons
;; (use-package all-the-icons
;;   :if (or (display-graphic-p) (daemonp)))

;; (use-package all-the-icons-completion
;;   :if (or (display-graphic-p) (daemonp))
;;   :init
;;   (all-the-icons-completion-mode)
;;   (add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup))

(use-package kind-icon
  :if (or (display-graphic-p) (daemonp))
  :after corfu
  :custom
  ;; Compute blended backgrounds
  (kind-icon-blend-background t)
  (kind-icon-default-face 'corfu-default)

  ;; Smaller height to fit with higher font sizes
  (kind-icon-default-style '(:padding 0 :stroke 0 :margin 0 :radius 0 :height 0.8 :scale 1.0 :background nil))

  ;; Change cache dir
  (svg-lib-icons-dir (no-littering-expand-var-file-name "svg-lib/cache/"))
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(provide 'icons-setup)
;;; icons-setup.el ends here
