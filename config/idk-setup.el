;;; idk-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the idk mode and configure it
;;; Code:

;; Better popping marks - from http://endlessparentheses.com/faster-pop-to-mark-command.html
(defun modi/multi-pop-to-mark (orig-fun &rest args)
  "When popping the mark, continue popping until the cursor actually does move.
Try the repeated popping up to 10 times.
ORIG-FUN is the original function.
ARGS are the arguments of that function."
  (let ((p (point)))
    (dotimes (i 10)
      (when (= p (point))
        (apply orig-fun args)))))
(advice-add 'pop-to-mark-command :around #'modi/multi-pop-to-mark)

;; Push a mark without activating transient-mode
(defun push-mark-no-activate ()
  "Pushes `point' to `mark-ring' and does not activate the region.
Equivalent to \\[set-mark-command] when \\[transient-mark-mode] is disabled"
  (interactive)
  (progn
    (push-mark (point) t nil)
    (message "Pushed mark to ring")))


(provide 'idk-setup)
;;; idk-setup.el ends here
