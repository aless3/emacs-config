;;; iedit-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 11/07/2023
;;; Edit multiple occurrences in the same way simultaneously
;;; Code:

(use-package iedit
  :config
  (global-unset-key (kbd "C--"))
  (global-set-key (kbd "C--") 'iedit-mode))


(provide 'iedit-setup)
;;; iedit-setup.el ends here
