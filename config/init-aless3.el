;;; init-aless3.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Inits everything
;;; Code:

(require 'base-setup)

(require 'package-managers-setup)
(require 'base-packages)
(require 'general-packages)

(require 'file-utils-packages)

(require 'language-checking-packages)

(require 'programming-packages)

(require 'xah-dvorak)

(unless block-fun
  (require 'fun-setup))

(provide 'init-aless3)
;;; init-aless3.el ends here
