;;; json-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the json utility modes and configure it
;;; Code:

;; Json-mode
(use-package json-snatcher)

(use-package json-mode
  :after json-snatcher
  :mode "\\.json\\'")

(provide 'json-setup)
;;; json-setup.el ends here
