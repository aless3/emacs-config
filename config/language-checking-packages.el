;;; language-checking-packages.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads language checking (fly* packages)
;;; Code:

(require 'flycheck-setup)
(require 'spell-fu-setup)
(require 'powerthesaurus-setup)

(provide 'language-checking-packages)
;;; language-checking-packages.el ends here
