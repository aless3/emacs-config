;;; lean-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 31/07/2024
;;; lean4 prover
;;; Code:

(use-package lean4-mode
  :ensure (lean4-mode
	         :type git
	         :host github
	         :repo "leanprover/lean4-mode"
	         :files ("*.el" "data"))
  ;; to defer loading the package until required
  :commands (lean4-mode))

(provide 'lean-setup)
;;; lean-setup.el ends here
