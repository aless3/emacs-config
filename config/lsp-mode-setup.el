;;; lsp-mode-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 31/07/2024
;;; lsp-mode & lsp-ui-mode for LSP functionalities
;;; Code:

(use-package lsp-mode
  :defer 5
  :custom
  (lsp-completion-provider :none) ;; we use Corfu!
  (lsp-warn-no-matched-clients nil)
  (lsp-modeline-code-actions-segments '(count icon name))
  :init
  (defun orderless-dispatch-flex-first (_pattern index _total)
    (and (eq index 0) 'orderless-flex))

  (defun lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless))
    ;; Configure the first word as flex filtered.
    (add-hook 'orderless-style-dispatchers #'orderless-dispatch-flex-first nil 'local)
    ;; Configure the cape-capf-buster.
    (setq-local completion-at-point-functions (list (cape-capf-buster #'lsp-completion-at-point))))
  :hook
  (prog-mode . lsp-mode)
  (lsp-completion-mode . lsp-mode-setup-completion))

(use-package lsp-ui
  :custom
  (lsp-ui-sideline-show-hover t)

  ;; disable currently unused lsp-ui services
  (lsp-ui-doc-enable nil)
  (lsp-ui-peek-enable nil)
  )

(provide 'lsp-mode-setup)
;;; lsp-mode-setup.el ends here
