;;; marginalia-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the marginalia mode and configure it
;;; Code:

;; Enable rich annotations using the Marginalia package
(use-package marginalia
  :init
  (marginalia-mode))

(provide 'marginalia-setup)
;;; marginalia-setup.el ends here
