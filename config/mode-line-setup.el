;;; mode-line-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 19/07/2023
;;; Configure a good mode line with moody and minions, also collapsing
;;; the minor modes to avoid cluttering automatically, but unluckily this
;;; does not work with the header line
;;; Code:

(use-package moody
  :if (or (display-graphic-p) (daemonp))
  :config
  (setq x-underline-at-descent-line t)
  (setq moody-mode-line-height 30)

  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (moody-replace-eldoc-minibuffer-message-function))

(use-package minions
  :config (minions-mode 1))

(provide 'mode-line-setup)
;;; mode-line-setup.el ends here
