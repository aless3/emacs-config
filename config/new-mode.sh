#!/bin/env sh
mode=$1
date="$(date +'%d/%m/%Y')"

file=";;; $mode-setup.el --- -*- lexical-binding: t -*-\n;;; Commentary:\n;;; File created by Alessandro Maria Benassi Trenta in date $date\n;;;\n;;; Code:\n\n\n\n(provide '$mode-setup)\n;;; $mode-setup.el ends here"

if [ "$#" = "1" ]
then
    echo -e $file > $mode-setup.el
    echo "Created file $mode-setup.el"
else
    echo "Wrong number of arguments, please pass only one argument as the name of the mode without the '-setup.el' suffix"
fi
