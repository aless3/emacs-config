;;; no-littering-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads no-littering, a package to keep clean the Emacs folder
;;; Code:

(use-package no-littering
  :config
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (add-to-list 'recentf-exclude no-littering-etc-directory)

  (setq auto-save-file-name-transforms
	      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))

(provide 'no-littering-setup)
;;; no-littering-setup.el ends here
