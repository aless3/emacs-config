;;; ocaml-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 15/04/2024
;;;
;;; Code:

;; this is too slow for now
;; (require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")

(use-package tuareg)
(use-package dune)
(use-package utop)

(provide 'ocaml-setup)
;;; ocaml-setup.el ends here
