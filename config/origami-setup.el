;;; origami-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the origami mode and configure it
;;; Code:

;; Origami folding
(use-package origami
  :ensure (origami :type git :host github :repo "elp-revive/origami.el")
  :config
  (global-origami-mode 1)
  (global-set-key (kbd "C-x /") 'origami-show-node)
  (global-set-key (kbd "C-x -") 'origami-close-node))

(provide 'origami-setup)
;;; origami-setup.el ends here
