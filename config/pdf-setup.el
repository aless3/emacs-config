;;; pdf-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads pdf reading modes and configure them
;;; Code:

(use-package pdf-tools
  :ensure (pdf-tools
           :type git :host github
           :repo "aikrahguzar/pdf-tools" :branch "upstream-pdf-roll"
           :pre-build (("./server/autobuild") ("make" "install-package"))
           :files (:defaults
                   "README"
                   ("Makefile")
                   ("build" "server")))
  :config
  (if (daemonp)
      (pdf-tools-install)
    (pdf-loader-install)))

(use-package saveplace-pdf-view)

;; For now we only install with the tar archive instead of building it
;; The archive is built with image-roll pull request
;; While waiting for this pr to be pulled, I build the tar myself
;; Original repo: `https://github.com/vedang/pdf-tools'
;; Original pull request: 'https://github.com/vedang/pdf-tools/pull/224'
;; (defun install-pdf-with-roll ()
;; "Install `pdf-tools-1.1.0.tar`'."
;; (interactive)
;; (package-install-file "./pdf-tools-1.1.0.tar")
;; (pdf-tools-install))

;; Line numbers mode is disabled because it crashes the pdf-mode
;; We also enable roll minor mode by default
(add-hook 'pdf-view-mode-hook
          #'(lambda ()
              (display-line-numbers-mode -1)
              (pdf-view-roll-minor-mode)))

;; Not sure if useful, for new commented
;; (setq TeX-view-program-selection '((output-pdf "pdf-tools")))
;; (setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))

(provide 'pdf-setup)
;;; pdf-setup.el ends here
