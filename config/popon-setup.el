;;; popon-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads popon
;;; Code:

(use-package popon
  :ensure (:type git :repo "https://codeberg.org/akib/emacs-popon.git"))

(provide 'popon-setup)
;;; popon-setup.el ends here
