;;; posframe-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads posframe
;;; Code:

(use-package posframe)

(provide 'posframe-setup)
;;; posframe-setup.el ends here
