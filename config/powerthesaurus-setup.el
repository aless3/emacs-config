;;; powerthesaurus-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 17/05/2023
;;;
;;; Code:

(use-package powerthesaurus
  :after xah-fly-keys
  :config
  (global-set-key (kbd "C-=") 'powerthesaurus-lookup-dwim))
(provide 'powerthesaurus-setup)
;;; powerthesaurus-setup.el ends here
