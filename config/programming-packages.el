;;; programming-packages.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads programming modes and utilities
;;; Code:

;; LSP et similia
;; (require 'lsp-bridge-setup)
(require 'corfu-setup)
(require 'lsp-mode-setup)

;; General
(require 'apheleia-setup)
;; (require 'origami-setup)
(require 'yasnippet-setup)
(require 'treesit-auto-setup)
(require 'markdown-setup)
(require 'diff-hl-setup)
(require 'expand-region-setup)
(require 'comment-config-setup)


;; magit yay
(require 'magit-setup)
;; (require 'treemacs-magit-setup)

;; Most used languages
(require 'python-setup)
(require 'slime-setup)
(require 'elixir-setup)

;; Debugger
;; (require 'realgud)

;; Extra languages
(require 'arduino-setup)
(require 'json-setup)
(require 'groovy-setup)
(require 'docker-setup)
(require 'ocaml-setup)
(require 'lean-setup)


(provide 'programming-packages)
;;; programming-packages.el ends here
