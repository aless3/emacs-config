;;; projectile-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the projectile mode and configure it
;;; Code:

;; Projectile
(use-package projectile
  :bind
  (("<f5>" . projectile-run-project)
   ("C-c e" . projectile-run-project)
   ("C-c C-e" . projectile-run-project)
   ("C-c p" . projectile-command-map))
  :init
  (projectile-mode +1)
  :config
  (add-to-list 'projectile-globally-ignored-directories "node_modules"))

(provide 'projectile-setup)
;;; projectile-setup.el ends here
