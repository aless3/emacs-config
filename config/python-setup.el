;;; python-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the python mode packages and configure it
;;; Code:

(use-package auto-virtualenv
  :init
  (use-package pyvenv
    :config
    (add-hook 'python-mode-hook 'pyvenv-mode)
    (add-hook 'python-ts-mode-hook 'pyvenv-mode)
    )
  :config
  (add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv)
  (add-hook 'projectile-after-switch-project-hook 'auto-virtualenv-set-virtualenv))

;; Anaconda mode
(require 'anaconda-setup)

(provide 'python-setup)
;;; python-setup.el ends here
