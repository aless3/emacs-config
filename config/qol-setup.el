;;; qol-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Configure various Quality of Life tweaks
;;; Code:

;; Quality of life emacs utils
(column-number-mode t)
(global-display-line-numbers-mode t)
(setq-local display-line-numbers-type 'relative)
(setq inhibit-startup-screen t)
(show-paren-mode t)
(electric-pair-mode 1)
(fset 'yes-or-no-p 'y-or-n-p) ;; y/n instead of yes/no

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode t)

;; Revert Dired and other buffers too
(setq global-auto-revert-non-file-buffers t)

;; Remember recently edited files, point and minibuffer history
(setq savehist-file "~/.emacs.d/var/savehist.el") ; compatibility savihist & no-littering
(setq recentf-filename-handlers nil)

(recentf-mode 1)
(save-place-mode 1)
(savehist-mode)

;; Vertical Scrolling
(setq scroll-step 1)
(setq maximum-scroll-margin 0.4)
(setq scroll-margin 25)
(setq scroll-conservatively 101)
(setq scroll-up-aggressively 0.01)
(setq scroll-down-aggressively 0.01)
(setq auto-window-vscroll nil)
(setq fast-but-imprecise-scrolling nil)
(setq mouse-wheel-scroll-amount '(3 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)

;; Horizontal Scrolling
(setq hscroll-step 1)
(setq hscroll-margin 1)

;; Make cursor the width of the character it is under. i.e. full width of a TAB.
(setq x-stretch-cursor t)

;; Makes *scratch* empty.
(setq initial-scratch-message "")
;; Removes *scratch* from buffer after the mode has been set.
(defun remove-scratch-buffer ()
  "Disable the scratch buffer when opening a file."
  (if (get-buffer "*scratch*")
      (kill-buffer "*scratch*")))
(add-hook 'elpaca-after-init-hook 'remove-scratch-buffer)

;; Remove useless whitespace before saving a file
(defun delete-trailing-whitespace-except-current-line ()
  "An alternative to `delete-trailing-whitespace'.

The original function deletes trailing whitespace of the current line."
  (interactive)
  (let ((begin (line-beginning-position))
        (end (line-end-position)))
    (save-excursion
      (when (< (point-min) (1- begin))
        (save-restriction
          (narrow-to-region (point-min) (1- begin))
          (delete-trailing-whitespace)
          (widen)))
      (when (> (point-max) (+ end 2))
        (save-restriction
          (narrow-to-region (+ end 2) (point-max))
          (delete-trailing-whitespace)
          (widen))))))

(defun smart-delete-trailing-whitespace ()
  "Invoke `delete-trailing-whitespace-except-current-line' on selected major modes only."
  (unless (member major-mode '(diff-mode))
    (delete-trailing-whitespace-except-current-line)))

(add-hook 'before-save-hook #'smart-delete-trailing-whitespace)

;; Replace selection on insert
(delete-selection-mode 1)

;; Map Alt key to Meta
(setq x-alt-keysym 'meta)

;; Map escape to keyboard quit (C-g)
(define-key key-translation-map (kbd "<escape>") (kbd "C-g"))

;; Better Compilation
(setq-default compilation-always-kill t) ; kill compilation process before starting another
(setq-default compilation-ask-about-save nil) ; save all buffers on `compile'

(defun where-am-i ()
  "An interactive function showing function `buffer-file-name' or `buffer-name'."
  (interactive)
  (message (kill-new (if (buffer-file-name) (buffer-file-name) (buffer-name)))))

;; Make <f11> toggle maximized instead of fullscreen (maximized is better because it is resizable)
(global-set-key (kbd "<f11>") nil)
(global-set-key (kbd "<f11>") 'toggle-frame-maximized)

;; More useful frame title, that show the buffer name
(setq frame-title-format "%b")

;; Move the backup files to user-emacs-directory/.backup
(setq backup-directory-alist `(("." . ,(expand-file-name ".backup" user-emacs-directory))))

;; Disable backups for root files
(setq backup-enable-predicate
      (lambda (name)
        (and (normal-backup-enable-predicate name)
             (not
              (let ((method (file-remote-p name 'method)))
                (when (stringp method)
                  (member method '("su" "sudo"))))))))

;; Make many backups to stay safe
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t
      backup-by-copying-when-linked t)

;; Add a newline automatically at the end of the file upon save.
(setq require-final-newline nil)
(setq mode-require-final-newline nil)

;; Automatically kill all active processes when closing Emacs
(setq confirm-kill-processes nil)

;; Turn Off Cursor Alarms
(setq ring-bell-function 'ignore)

;; Show Keystrokes in Progress Instantly
(setq echo-keystrokes 0.1)

;; Indentation tweaks
(setq-default indent-tabs-mode nil)
;; (setq-default indent-line-function 'insert-tab)
(setq-default tab-width 2)
(setq-default c-basic-offset 2)
(setq-default java-ts-mode-indent-offset 2)
(setq-default js-switch-indent-offset 2)
(setq-default tab-always-indent 'complete)

;; Open some large files without warning
(defvar ok-large-file-types
  (rx "." (or "mp4" "mkv" "pdf") string-end)
  "Regexp matching extensions that ignore `large-file-warning-threshold'.")

(define-advice abort-if-file-too-large
    (:around (orig-fn size op-type filename &optional offer-raw) ok-large-file-types)
  "If FILENAME matches `ok-large-file-types', do not abort."
  (unless (string-match-p ok-large-file-types filename)
    (funcall orig-fn size op-type filename offer-raw)))

;; Better garbage collection - in early-init it was set super high to speed up initialization
;; If you experience freezing, decrease this.  If you experience stuttering, increase this
;; Now testing 67108864 (64mb), previously was 134217728 (128mb)
(defvar better-gc-cons-threshold 67108864 ; 64mb
  "The default value to use for `gc-cons-threshold'.")

(add-hook 'elpaca-after-init-hook
          (lambda ()
            (setq gc-cons-threshold better-gc-cons-threshold)))

;; Increase the amount of data which Emacs reads from the process
;; the emacs default is too low 4k considering that the some of the
;; language server responses are in 800k - 3M range
(setq read-process-output-max (* 1024 1024 8)) ;; 8mb

(global-subword-mode)

;; enable updating built in packages, mostly magit often needs those updates
(setq package-install-upgrade-built-in t)

(provide 'qol-setup)
;;; qol-setup.el ends here
