;;; rainbow-delimiter-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Highlight parentheses with different color
;;; Code:

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(provide 'rainbow-delimiter-setup)
;;; rainbow-delimiter-setup.el ends here
