;;; realgud-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the realgud debugger interface
;;; Code:

(use-package realgud)

(provide 'realgud-setup)
;;; realgud-setup.el ends here
