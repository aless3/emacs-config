;;; sideline-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads packages to display useful info in the sideline
;;; Code:

;; (use-package sideline
;;   :hook (flycheck-mode . sideline-mode)
;;   :init
;;   (setq sideline-lsp-ignore-duplicate t)
;;   (setq sideline-display-backend-name t)
;;   (setq sideline-delay 0.1)
;;   (setq sideline-backends-right '(sideline-flycheck))
;;   :config
;;   (global-sideline-mode))


;; (use-package sideline-flycheck
;;   :hook (flycheck-mode . sideline-flycheck-setup))

(provide 'sideline-setup)
;;; sideline-setup.el ends here
