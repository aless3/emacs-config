;;; slime-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the slime mode and configure it
;;; Code:

;; Slime using SBCL
(use-package sly
  :defer t
  :init
  (setq inferior-lisp-program "sbcl")
  :config
  ;; (slime-mode)
  (cond ((boundp 'xah-fly-command-map)
         (define-key xah-fly-command-map (kbd "SPC . s") 'sly)
         (define-key xah-fly-command-map (kbd "SPC . c") 'sly-eval-buffer)))
  ;; :hook
  ;; (lisp-mode . (lambda ()
                 ;; (unless (get-process "SLIME Lisp")
                   ;; (let ((oldbuff (current-buffer)))
                     ;; (slime)
                     ;; )))
  ;; (slime-mode . (lambda ()
                  ;; (add-to-list 'slime-contribs 'slime-fancy)
                  ;; (add-to-list 'slime-contribs 'inferior-slime)))
  )

(provide 'slime-setup)
;;; slime-setup.el ends here
