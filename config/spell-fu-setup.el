;;; spell-fu-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads spell-fu - spell checking
;;; Code:

(use-package spell-fu
  :config
  (spell-fu-global-mode))

(provide 'spell-fu-setup)
;;; spell-fu-setup.el ends here
