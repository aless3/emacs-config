;;; switch-window-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads switch-window to switch between windows easily.
;;; This is meant to replace ace-window
;;; Code:

(use-package switch-window
  :config
  (global-set-key (kbd "C-x o") 'switch-window)
  (setq switch-window-background t)

  (define-key switch-window-extra-map "i" nil)
  (define-key switch-window-extra-map "j" nil)
  (define-key switch-window-extra-map "k" nil)
  (define-key switch-window-extra-map "l" nil)

  (setq switch-window-threshold 2)  (setq switch-window-qwerty-shortcuts '("a" "o" "e" "u" "i" "d" "h" "t" "n" "s"))
  (setq switch-window-shortcut-style 'qwerty))

(provide 'switch-window-setup)
;;; switch-window-setup.el ends here
