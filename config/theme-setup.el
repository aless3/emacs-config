;;; theme-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the theme and configure it
;;; Currently using modus-vivendi dark theme
;;; Code:

;; Theme - modus vivendi theme
(use-package modus-themes
  :init
  ;; Add all your customizations prior to loading the themes
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs nil
        modus-themes-region '(bg-only no-extend))
  :config
  ;; Load the dark theme (modus-vivendi)
  (load-theme 'modus-vivendi :no-confim))

(provide 'theme-setup)
;;; theme-setup.el ends here
