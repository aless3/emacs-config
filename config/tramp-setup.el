;;; tramp-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the tramp mode and configure it
;;; Code:

(use-package tramp
  :defer 1
  :config
  (setq tramp-allow-unsafe-temporary-files t)
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)

  ;; dirvish integration
  (setq tramp-chunksize 2000)
  (setq tramp-use-ssh-controlmaster-options nil)
  ;; (add-to-list 'tramp-connection-properties
  ;; (list (regexp-quote "/ssh:YOUR_HOSTNAME:")
  ;; "direct-async-process" t))

  ;; TRAMP gcloud ssh
  (add-to-list 'tramp-methods
               '("gssh"
                 (tramp-login-program        "gcloud compute ssh")
                 (tramp-login-args           (("%h")))
                 (tramp-async-args           (("-q")))
                 (tramp-remote-shell         "/bin/bash")
                 (tramp-remote-shell-args    ("-c"))
                 (tramp-gw-args              (("-o" "GlobalKnownHostsFile=/dev/null")
                                              ("-o" "UserKnownHostsFile=/dev/null")
                                              ("-o" "StrictHostKeyChecking=no")))
                 (tramp-default-port         22))))

(provide 'tramp-setup)
;;; tramp-setup.el ends here
