;;; tree-sitter-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads tree-sitter and setup it
;;; Code:

;; (use-package tree-sitter
;; :config
;; (setq treesit-font-lock-level 4)
;; (global-tree-sitter-mode)
;; (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

;; (use-package tree-sitter-langs)

(add-to-list 'load-path "~/.emacs.d/tree-sitter/")

(require 'treesit-auto-setup)

(provide 'tree-sitter-setup)
;;; tree-sitter-setup.el ends here
