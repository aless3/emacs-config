;;; treemacs-magit-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the treemacs-magit mode and configure it
;;; Code:

;; Treemacs + Magit
(use-package treemacs-magit
  :defer t
  :after (treemacs magit))

(provide 'treemacs-magit-setup)
;;; treemacs-magit-setup.el ends here
