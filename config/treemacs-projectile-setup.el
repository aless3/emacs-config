;;; treemacs-projectile-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the treemacs-projectile mode and configure it
;;; Code:

;; Treemacs + projectile
(use-package treemacs-projectile
  :after (treemacs projectile)
  :defer t)

(provide 'treemacs-projectile-setup)
;;; treemacs-projectile-setup.el ends here
