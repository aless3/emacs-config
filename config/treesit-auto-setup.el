;;; treesit-auto-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads treesit-auto
;;; Code:

(use-package treesit-auto
  :config
  (setq treesit-auto-install t)
  (setq treesit-font-lock-level 4)
  (global-treesit-auto-mode))


(provide 'treesit-auto-setup)
;;; treesit-auto-setup.el ends here
