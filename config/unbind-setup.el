;;; unbind-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;: Unbind unneeded or harmful keys
;;; Code:

(global-set-key (kbd "C-z") nil)
(global-set-key (kbd "M-z") nil)
(global-set-key (kbd "M-m") nil)
(global-set-key (kbd "C-x C-z") nil)
(global-set-key (kbd "M-/") nil)
(global-set-key (kbd "C-v") nil)
(global-set-key (kbd "M-v") nil)
(global-set-key (kbd "M-;") nil)
(global-set-key (kbd "M-y") nil)
(global-set-key (kbd "C-x m") nil)
(global-set-key (kbd "C--") nil)
(global-set-key (kbd "C-=") nil)
(global-set-key (kbd "C-x f") nil)

;; Unbind insert (I hate it)
(global-set-key (kbd "<insert>") nil)
(global-set-key (kbd "<insertchar>") nil)

(provide 'unbind-setup)
;;; unbind-setup.el ends here
