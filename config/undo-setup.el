;;; undo-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the undo modes and configure them
;;; Code:

;; Undo packages (undo-fu based)
(use-package undo-fu
  :config
  ;; Give more space to store undo info
  (setq undo-limit 8388608) ;; 8mb. ; testing: prev was 67108864
  (setq undo-strong-limit 16777216) ;; 16mb. ; testing: prev was 100663296
  (setq undo-outer-limit 33554432) ;; 32mb. ; testing: prev was 1006632960

  ;; Basic key bindings
  (global-set-key (kbd "C-_") 'undo-fu-only-undo)
  (global-set-key (kbd "M-_") 'undo-fu-only-redo)
  (global-set-key (kbd "C-/") 'undo-fu-only-undo)
  (global-set-key (kbd "M-/") 'undo-fu-only-redo))

;; Keep history between emacs sessions
(use-package undo-fu-session
  :hook (after-init . global-undo-fu-session-mode)
  :config
  (setq undo-fu-session-compression 'zst)
  (setq undo-fu-session-incompatible-files '("/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'")))

;; Vundo - undo enhancement
(use-package vundo
  :config
  (setq vundo-compact-display t)
  (custom-set-faces
   '(vundo-node ((t (:foreground "#808080"))))
   '(vundo-stem ((t (:foreground "#808080"))))
   '(vundo-highlight ((t (:foreground "#FFFF00")))))

  ;; Use `HJKL` VIM-like motion, also Home/End to jump around.
  ;; But link xah fly keys in dvorak instead of `hjkl`
  (define-key vundo-mode-map (kbd "n") #'vundo-forward)
  (define-key vundo-mode-map (kbd "<right>") #'vundo-forward)
  (define-key vundo-mode-map (kbd "h") #'vundo-backward)
  (define-key vundo-mode-map (kbd "<left>") #'vundo-backward)
  (define-key vundo-mode-map (kbd "t") #'vundo-next)
  (define-key vundo-mode-map (kbd "<down>") #'vundo-next)
  (define-key vundo-mode-map (kbd "c") #'vundo-previous)
  (define-key vundo-mode-map (kbd "<up>") #'vundo-previous)

  (define-key vundo-mode-map (kbd "<home>") #'vundo-stem-root)
  (define-key vundo-mode-map (kbd "<end>") #'vundo-stem-end)

  (define-key vundo-mode-map (kbd "q") #'vundo-quit)
  (define-key vundo-mode-map (kbd "C-g") #'vundo-quit)
  (define-key vundo-mode-map (kbd "<escape>") #'vundo-quit)
  (define-key vundo-mode-map (kbd "RET") #'vundo-confirm)
  (define-key vundo-mode-map (kbd "SPC") #'vundo-confirm)
  :bind ("C-x u" . vundo))

(provide 'undo-setup)
;;; undo-setup.el ends here
