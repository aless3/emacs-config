;;; vertico-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the vertico mode and configure it
;;; Code:

(use-package vertico
  :ensure (:files (:defaults "extensions/*")
                    :includes (vertico-indexed
                                vertico-grid ;
                                vertico-mouse ;
                                vertico-quick
                                vertico-repeat ;
                                vertico-directory ;
                                vertico-multiform ;
                                vertico-indexed
                                ))
  :init
  (vertico-mode)
  ;; Extensions
  ;; Enable vertico-multiform
  (vertico-multiform-mode)
  ;; Configure the display per completion category.
  ;; Use the grid display for files and a buffer
  ;; for the consult-grep commands.
  (setq vertico-multiform-categories
      '((file grid)
        (consult-grep buffer consult-ag)))

  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))

  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(provide 'vertico-setup)
;;; vertico-setup.el ends here
