;;; vterm-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the vterm mode, a terminal emulator and configure it
;;; Code:

;; Terminal emulator
(use-package vterm
  :config
  (setq vterm-always-compile-module t)
  (setq vterm-kill-buffer-on-exit t)
  (define-key vterm-mode-map (kbd "C--") #'vterm-send-next-key)
  (define-key vterm-mode-map (kbd "<C-backspace>")
              (lambda () (interactive) (vterm-send-key (kbd "C-w")))))

(provide 'vterm-setup)
;;; vterm-setup.el ends here
