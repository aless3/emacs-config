;;; which-key-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the which-key mode and configure it
;;; Code:

;; Which-key
(use-package which-key
  :config
  (which-key-mode))

(provide 'which-key-setup)
;;; which-key-setup.el ends here
