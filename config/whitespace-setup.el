;;; whitespace-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the whitespace mode and configure it
;;; Code:

;; Whitespace
;; (use-package whitespace)

(provide 'whitespace-setup)
;;; whitespace-setup.el ends here
