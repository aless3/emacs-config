;;; windmove-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the windmove mode and configure it
;;; Code:

;; Windmove - switch between buffers
(use-package windmove
  :config
  ;; use shift + arrow keys to switch between visible buffers
  (windmove-default-keybindings))

(provide 'windmove-setup)
;;; windmove-setup.el ends here
