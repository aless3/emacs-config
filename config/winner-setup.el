;;; winner-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the winner mode and configure it
;;; Code:

;; Winner - buffer management
(use-package winner
  :custom
  (winner-boring-buffers
   '("*Completions*"
     "*Compile-Log*"
     "*inferior-lisp*"
     "*Fuzzy Completions*"
     "*Apropos*"
     "*Help*"
     "*cvs*"
     "*Buffer List*"
     "*Ibuffer*"
     "*esh command on file*"))
  :config
  (winner-mode 1))

(provide 'winner-setup)
;;; winner-setup.el ends here
