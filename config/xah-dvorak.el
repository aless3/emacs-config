;;; xah-dvorak.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads xah-fly-keys on dvorak and swap some keys for
;;; for better ergonomics on dvorak
;;; Code:

;; In config/constats-setup.el there is a constant
;; called 'block-xah' which abstracts xah-dvorak usage

(unless block-xah
  (require 'dvorak-translation-setup)
  (require 'xah-setup))

(provide 'xah-dvorak)
;;; xah-dvorak.el ends here
