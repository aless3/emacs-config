;;; xah-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads the xah-fly-keys mode and configure it, also
;;: Define some utility functions to work with words and s-exp
;;; Code:

(use-package xah-fly-keys
  ;; Using this package made useless other parts of code,
  ;; but the valuable parts that have been removed are kept in disabled.el
  :config
  (xah-fly-keys-set-layout "dvorak")
  (setq xah-fly-use-control-key nil)
  (setq xah-fly-use-meta-key nil)

  ;; store less positions for better use of jump
  (setq mark-ring-max 6)
  (setq global-mark-ring-max 6)

  ;; Utility functions
  ;; words
  (defun delete-backward-word (arg)
    "Delete characters backward until encountering the beginning of a word.
With argument ARG, do this that many times."
    (interactive "p")
    (delete-region (point) (progn (backward-word arg) (point))))


  (defun delete-forward-word (arg)
    "Delete characters forward until encountering the end of a word.
With argument ARG, do this that many times."
    (interactive "p")
    (delete-region (point) (progn (forward-word arg) (point))))


  ;; parens
  (defun a3-delete-forward-paren-content()
    "Delete the content from point to the end of the
sexp."
    (interactive)
    (let (($p0 (point)))
      (forward-sexp)
      (delete-region $p0 (point))))


  (defun a3-delete-backward-paren-content()
    "Delete the content from the start of the last
sexp to point."
    (interactive)
    (let (($p0 (point)))
      (forward-sexp -1)
      (delete-region (point) $p0)))


  ;; char or brackets
  (defun a3-delete-forward-char-or-paren-maybe-content(ARG)
    (interactive "P")
    (let ((syntax-of-next (char-syntax (char-after))))
      (cond ((eq syntax-of-next 40)
             (message "Open Parenthesis Characters")
             (a3-delete-forward-paren-content))

            ((eq syntax-of-next 41)
             (message "Close Parenthesis Characters")
             (forward-char)
             (unless ARG
               (a3-delete-backward-paren-content)))

            (t (hungry-delete-forward 1)))))


  (defun a3-delete-backward-char-or-paren-maybe-content(ARG)
    (interactive "P")
    (let ((syntax-of-next (char-syntax (char-before))))
      (cond ((eq syntax-of-next 41)
             (message "Close Parenthesis Characters")
             (a3-delete-backward-paren-content))

            ((eq syntax-of-next 40)
             (message "Open Parenthesis Characters")
             (backward-char)
             (unless ARG
               (a3-delete-forward-paren-content)))

            (t (hungry-delete-backward 1)))))


  ;; marking
  (defun a3-push-mark (&optional keep)
    "Push mark at point, then deactivate transient mode to stop selection."
    (interactive)
    (push-mark-command nil)
    (deactivate-mark))

  ;; single key
  (define-key xah-fly-command-map (kbd "1") 'delete-other-windows)
  (define-key xah-fly-command-map (kbd "2") 'split-window-below)

  (define-key xah-fly-command-map (kbd "3") 'split-window-right)
  (define-key xah-fly-command-map (kbd "4") 'delete-window)

  (define-key xah-fly-command-map (kbd "5") 'mark-sexp) ;; ???

  (define-key xah-fly-command-map (kbd "8") 'er/expand-region)

  (define-key xah-fly-command-map (kbd "b") 'consult-line)
  (define-key xah-fly-command-map (kbd "d") 'beginning-of-line)
  (define-key xah-fly-command-map (kbd "f") 'undo-fu-only-undo)

  (define-key xah-fly-command-map (kbd "k") 'yank) ;; paste
  (define-key xah-fly-command-map (kbd "j") 'whole-line-or-region-kill-ring-save) ;; copy
  (define-key xah-fly-command-map (kbd "q") 'whole-line-or-region-kill-region) ;; cut

  (define-key xah-fly-command-map (kbd "s") 'end-of-line)
  (define-key xah-fly-command-map (kbd "w") 'switch-window)
  (define-key xah-fly-command-map (kbd "z") 'embark-act)

  (define-key xah-fly-command-map (kbd "e") 'a3-delete-forward-char-or-paren-maybe-content)
  (define-key xah-fly-command-map (kbd "o") 'a3-delete-backward-char-or-paren-maybe-content)

  (define-key xah-fly-command-map (kbd ".") 'delete-forward-word)
  (define-key xah-fly-command-map (kbd ",") 'delete-backward-word)
  (define-key xah-fly-command-map (kbd "'") 'avy-goto-char-timer)

  (define-key xah-fly-command-map (kbd "p") 'universal-argument)

  (define-key xah-fly-command-map (kbd ";") 'evilnc-comment-or-uncomment-lines)

  (define-key xah-fly-command-map (kbd "/") 'xah-pop-local-mark-ring)
  (define-key xah-fly-command-map (kbd "?") 'a3-push-mark)
  (define-key xah-fly-command-map (kbd "-") 'a3-push-mark)

  (define-key xah-fly-command-map (kbd "[") 'flycheck-previous-error)
  (define-key xah-fly-command-map (kbd "]") 'flycheck-next-error)

  ;; shift + key OR long keypress (kanata & qmk)
  (define-key xah-fly-command-map (kbd "C") 'windmove-up)
  (define-key xah-fly-command-map (kbd "H") 'windmove-left)
  (define-key xah-fly-command-map (kbd "T") 'windmove-down)
  (define-key xah-fly-command-map (kbd "N") 'windmove-right)

  (define-key xah-fly-command-map (kbd "K") 'consult-yank-from-kill-ring)
  (define-key xah-fly-command-map (kbd "F") 'undo-fu-only-redo)
  (define-key xah-fly-command-map (kbd "Y") 'consult-mark)

  ;; control + key
  (define-key xah-fly-command-map (kbd "C-u") 'backward-paragraph) ;; C-u (match-end )ans C-c with the translation map
  (define-key xah-fly-command-map (kbd "C-d") 'previous-buffer)
  (define-key xah-fly-command-map (kbd "C-s") 'next-buffer)
  (define-key xah-fly-command-map (kbd "C-t") 'forward-paragraph)
  (define-key xah-fly-command-map (kbd "C-z") 'embark-dwim)

  ;; space + (keys)
  (define-key xah-fly-command-map (kbd "SPC f") nil)
  (define-key xah-fly-command-map (kbd "SPC u") 'consult-buffer)

  (define-key xah-fly-command-map (kbd "SPC =") 'powerthesaurus-lookup-dwim)

  (define-key xah-fly-command-map (kbd "SPC t w") 'switch-window)

  ;; (define-key xah-fly-command-map (kbd "SPC . c") 'vterm)
  (define-key xah-fly-command-map (kbd "SPC . e") 'consult-line)
  (define-key xah-fly-command-map (kbd "SPC . p") nil)
  (define-key xah-fly-command-map (kbd "SPC . t") 'vterm)
  (define-key xah-fly-command-map (kbd "SPC . u") nil)

  ;; insert map
  (global-set-key (kbd "C-t") 'xah-insert-paren)
  (global-set-key (kbd "C-r") 'xah-insert-square-bracket)
  (global-set-key (kbd "C-u") 'xah-insert-brace) ;; will be on C-c due translation map, see `dvorak-translation-setup.el`

  (xah-fly-keys 1)
  (defun emacsclient-start-command-map (frame)
    (progn
      (select-frame frame)
      (xah-fly-command-mode-activate-no-hook))
    )
  (if (daemonp)
      (add-hook 'after-make-frame-functions 'emacsclient-start-command-map))
  
  ;; Re-enable some bindings lost due to xah-fly-keys takeover
  (global-set-key (kbd "C--") 'iedit-mode))



(provide 'xah-setup)
;;; xah-setup.el ends here
