;;; xkcd-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Read XKCD comics from Emacs
;;; Code:

(use-package xkcd)

(provide 'xkcd-setup)
;;; xkcd-setup.el ends here
