;;; zoom-setup.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 02/12/2022
;;; Loads zoom to keep window sizes balanced
;;; Code:

(use-package zoom
  :config
  (defun size-callback ()
    (cond ((> (frame-pixel-width) 1280) '(90 . 0.75))
          (t                            '(0.5 . 0.5))))
  (zoom-mode t)
  :custom
  (zoom-size 'size-callback)
  (zoom-ignored-major-modes '(dired-mode markdown-mode))
  (zoom-ignored-buffer-names '("zoom.el" "init.el"))
  (zoom-ignored-buffer-name-regexps '("^*calc"))
  (zoom-ignored-buffer-name-regexps '("*.+*"))
  (zoom-ignore-predicates '((lambda () (> (count-lines (point-min) (point-max)) 20)))))

(provide 'zoom-setup)
;;; zoom-setup.el ends here
