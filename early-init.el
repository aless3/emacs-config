;;; early-init.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 30/10/2022
;;; Some optimizations to speed up the initialization
;;; Code:

;; Change the location of the native compilation
(when (fboundp 'startup-redirect-eln-cache)
  (defvar native-comp-eln-load-path nil
    "Define it if is not already defined to avoid errors.
Setting to nil because it will be updated by the next instruction.")
  (startup-redirect-eln-cache
   (convert-standard-filename
	  (expand-file-name  "var/eln-cache/" user-emacs-directory))))
;; and prefer newer files
(setq load-prefer-newer noninteractive)

;; DeferGC
(setq gc-cons-threshold 100000000)

;; No byte-compilation warnings
(setq native-comp-async-report-warnings-errors 'silent) ;; native-comp warning
(setq byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local))

;; Byte compilation optimization
(setq native-comp-speed 4)

;; We handle package initialization in the init
(setq package-enable-at-startup nil)

(setq frame-resize-pixelwise t)

(modify-all-frames-parameters
 '((fullscreen . maximized)

   ;; Also disable scroll bars
   (vertical-scroll-bars . nil)
   (horizontal-scroll-bars . nil)

   ;; Setting the face in here prevents flashes of
	 ;; color as the theme gets activated
   (background-color . "#000000")
	 (ns-appearance . dark)
	 (ns-transparent-titlebar . t)))

;; Remove useless bars
(menu-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)

;; Don't pop up UI dialogs when prompting
(setq use-dialog-box nil)

(setenv "LSP_USE_PLISTS" "true")

(provide 'early-init)
;;; early-init.el ends here
