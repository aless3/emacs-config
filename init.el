;;; init.el --- -*- lexical-binding: t -*-
;;; Commentary:
;;; File created by Alessandro Maria Benassi Trenta in date 05/12/2020
;;; Loads the various useful modes
;;; Code:

(add-to-list 'load-path "~/.emacs.d/config/")
(require 'init-aless3)

(provide 'init)
;;; init.el ends here
